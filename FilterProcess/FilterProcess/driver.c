#include <fltKernel.h>

#define MyDbgPrint(format, ...) \
	DbgPrintEx(DPFLTR_SYSTEM_ID, DPFLTR_ERROR_LEVEL, (format), __VA_ARGS__);

DRIVER_INITIALIZE DriverEntry;
DRIVER_UNLOAD DriverUnload;

LARGE_INTEGER CallbackRegistryKeyRenamedCookie;

VOID
CallbackProcessCreated(
    _Inout_     PEPROCESS              Process,
    _In_        HANDLE                 ProcessId,
    _Inout_opt_ PPS_CREATE_NOTIFY_INFO CreateInfo
);

_IRQL_requires_same_
_Function_class_(EX_CALLBACK_FUNCTION)
NTSTATUS
CallbackRegistryKeyRenamed(
    _In_     PVOID CallbackContext,
    _In_opt_ PVOID Argument1,
    _In_opt_ PVOID Argument2
);

VOID
CallbackLoadDriverImage(
    _In_opt_ PUNICODE_STRING FullImageName,
    _In_     HANDLE          ProcessId,
    _In_     PIMAGE_INFO     ImageInfo
);

NTSTATUS
DriverEntry(
	_In_ PDRIVER_OBJECT	 DriverObject,
	_In_ PUNICODE_STRING RegistryPath
)
{
	NTSTATUS       ntStatus = STATUS_SUCCESS;

	UNREFERENCED_PARAMETER(RegistryPath);

	DriverObject->DriverUnload = DriverUnload;

    ntStatus = PsSetCreateProcessNotifyRoutineEx(CallbackProcessCreated, FALSE);
    if (!NT_SUCCESS(ntStatus))
    {
        MyDbgPrint("PsSetCreateProcessNotifyRoutingEx error %ld", ntStatus);
        return ntStatus;
    }

    ntStatus = CmRegisterCallback(
        CallbackRegistryKeyRenamed,
        NULL,
        &CallbackRegistryKeyRenamedCookie
    );
    if (!NT_SUCCESS(ntStatus))
    {
        MyDbgPrint("CmRegisterCallback error %ld", ntStatus);
        return ntStatus;
    }

    ntStatus = PsSetLoadImageNotifyRoutine(CallbackLoadDriverImage);
    if (!NT_SUCCESS(ntStatus))
    {
        MyDbgPrint("PsSetLoadImageNotifyRoutine error %ld", ntStatus);
        return ntStatus;
    }

	return ntStatus;
}

VOID
DriverUnload(
    _In_ PDRIVER_OBJECT DriverObject
)
{
    UNREFERENCED_PARAMETER(DriverObject);

    PAGED_CODE();

    PsSetCreateProcessNotifyRoutineEx(CallbackProcessCreated, TRUE);
    CmUnRegisterCallback(CallbackRegistryKeyRenamedCookie);
    PsRemoveLoadImageNotifyRoutine(CallbackLoadDriverImage);
}

VOID
CallbackProcessCreated(
    _Inout_     PEPROCESS              Process,
    _In_        HANDLE                 ProcessId,
    _Inout_opt_ PPS_CREATE_NOTIFY_INFO CreateInfo
)
{
    UNREFERENCED_PARAMETER(Process);

    if (CreateInfo == NULL)
    {
        // Process is exiting, nothing to do
        return;
    }

    MyDbgPrint("Process %p created\n", ProcessId);
    if (CreateInfo->ImageFileName != NULL)
    {
        MyDbgPrint("ImageFileName = %wZ\n", CreateInfo->ImageFileName);
    }
    if (CreateInfo->CommandLine != NULL)
    {
        MyDbgPrint("CommandLine = %wZ\n\n", CreateInfo->CommandLine);
    }
}

_IRQL_requires_same_
_Function_class_(EX_CALLBACK_FUNCTION)
NTSTATUS
CallbackRegistryKeyRenamed(
    _In_     PVOID CallbackContext,
    _In_opt_ PVOID Argument1,
    _In_opt_ PVOID Argument2
)
{
    NTSTATUS ntStatus = STATUS_SUCCESS;

    UNREFERENCED_PARAMETER(CallbackContext);

    if (Argument1 != (PVOID)RegNtPreRenameKey || Argument2 == NULL)
    {
        // Nothing to do
        return ntStatus;
    }

    PREG_RENAME_KEY_INFORMATION information = (PREG_RENAME_KEY_INFORMATION)Argument2;
    PCUNICODE_STRING registryKeyPath;

    ntStatus = CmCallbackGetKeyObjectIDEx(
        &CallbackRegistryKeyRenamedCookie,
        information->Object,
        NULL,
        &registryKeyPath,
        0);
    if (!NT_SUCCESS(ntStatus))
    {
        MyDbgPrint("CmCallbackGetKeyObjectIDEx error %ld", ntStatus);
        return ntStatus;
    }

    MyDbgPrint("Registry key %wZ renamed to %wZ\n", registryKeyPath, information->NewName);

    return ntStatus;
}

VOID
CallbackLoadDriverImage(
    _In_opt_ PUNICODE_STRING FullImageName,
    _In_     HANDLE          ProcessId,
    _In_     PIMAGE_INFO     ImageInfo
)
{
    UNREFERENCED_PARAMETER(ImageInfo);

    if (ProcessId != NULL)
    {
        // Image is not a driver, nothing to do
        return;
    }

    MyDbgPrint("Driver image loaded: %wZ\n", FullImageName);
}
