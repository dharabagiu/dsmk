//
//   Author(s)    : Radu PORTASE(rportase@bitdefender.com)
//
#include "ThreadFilter.h"
#include "Communication.h"
#include "CommShared.h"
#include "Trace.h"
#include "ThreadFilter.tmh"

static VOID
ThreadFltSendMessageThreadOperation(
    _In_ HANDLE ProcessId,
    _In_ HANDLE ThreadId,
    _In_ BOOLEAN Create
)
{
    MY_DRIVER_THREAD_MSG msg;
    msg.Header.MessageCode = Create ? msgThreadCreate : msgThreadTerminate;

    msg.ProcessId = HandleToULong(ProcessId);
    msg.ThreadId = HandleToULong(ThreadId);

    ULONG replySize = 0;
    NTSTATUS status = CommSendMessage(&msg, sizeof(msg), NULL, &replySize);
    if (!NT_SUCCESS(status))
    {
        LogError("CommSendMessage failed with status = 0x%X", status);
    }
}

static VOID
ThreadFltNotifyRoutine(
    _In_ HANDLE ProcessId,
    _In_ HANDLE ThreadId,
    _In_ BOOLEAN Create
)
{
    ThreadFltSendMessageThreadOperation(ProcessId, ThreadId, Create);
}

NTSTATUS
ThreadFltInitialize()
{
    return PsSetCreateThreadNotifyRoutine(ThreadFltNotifyRoutine);
}

NTSTATUS
ThreadFltUninitialize()
{
    return PsRemoveCreateThreadNotifyRoutine(ThreadFltNotifyRoutine);
}
