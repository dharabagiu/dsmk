#include <Windows.h>
#include <Psapi.h>
#include <tchar.h>

#include "dmon.h"
#include "threadpool.h"
#include "logger.h"

#define NUMBER_OF_THREADS 5
#define MAX_PROCESS_COUNT 4096
#define MAX_PROCESS_NAME 1024
#define MAX_ERROR_STR 128
#define IOCTL_PROTECT_PROCESS CTL_CODE(FILE_DEVICE_UNKNOWN, 0xd80, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define DRIVER_PATH "\\\\.\\DmonDriver"

static BOOL g_DmonStarted = FALSE;
static PTHREAD_POOL g_ThreadPool = NULL;

BOOL DmonIsRunning()
{
    return g_DmonStarted;
}

BOOL StartDmon()
{
    if (g_DmonStarted)
    {
        return FALSE;
    }

    g_ThreadPool = ThreadPoolInit(NUMBER_OF_THREADS);
    if (NULL == g_ThreadPool)
    {
        return FALSE;
    }

    g_DmonStarted = TRUE;

    LogInfo("Dmon started");

    return TRUE;
}

void StopDmon()
{
    if (!g_DmonStarted)
    {
        return;
    }

    g_DmonStarted = FALSE;
    ThreadPoolUninit(g_ThreadPool);

    LogInfo("Dmon stopped");
}

BOOL PrintRunningProcesses(FILE* file)
{
    DWORD processes[1024], processesLength;
    if (!EnumProcesses(processes, sizeof(processes), &processesLength))
    {
        LogError("Error while calling EnumProcesses: %u", GetLastError());
        return FALSE;
    }

    _ftprintf_s(file, "\n");

    DWORD numProcesses = processesLength / sizeof(DWORD);
    for (int i = 0; i != numProcesses; ++i)
    {
        TCHAR processName[MAX_PROCESS_NAME] = TEXT("<unknown>");

        if (0 == processes[i])
        {
            LogWarning("Process id is zero");
            continue;
        }

        HANDLE process = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, processes[i]);
        if (NULL == process)
        {
            LogWarning("Error while calling OpenProcess: %u", GetLastError());
        }
        else
        {
            HMODULE module;
            DWORD modulesLength;
            if (!EnumProcessModules(process, &module, sizeof(module), &modulesLength))
            {
                LogWarning("Error while calling EnumProcessModules: %u", GetLastError());
            }
            else
            {
                if (!GetModuleBaseName(process, module, processName, sizeof(processName) / sizeof(TCHAR)))
                {
                    LogWarning("Error while calling GetModuleBaseName: %u", GetLastError());
                }
            }
        }

        _ftprintf_s(file, "%8u    %s\n", processes[i], processName);
    }

    _ftprintf_s(file, "\n");

    return TRUE;
}

BOOL ProtectProcess(HANDLE processId)
{
    HANDLE hDevice = CreateFile(
        DRIVER_PATH,
        GENERIC_READ | GENERIC_WRITE,
        FILE_SHARE_READ | FILE_SHARE_WRITE,
        NULL,
        OPEN_EXISTING,
        FILE_FLAG_OVERLAPPED,
        NULL);

    if (hDevice == INVALID_HANDLE_VALUE)
    {
        LogWarning("Error while calling CreateFile: %u\n", GetLastError());
        return FALSE;
    }

    BOOL result = DeviceIoControl(
        hDevice,
        IOCTL_PROTECT_PROCESS,
        &processId,
        sizeof(processId),
        NULL,
        0,
        NULL,
        NULL);
    if (!result)
    {
        LogWarning("Error while calling DeviceIoControl: %u\n", GetLastError());
        CloseHandle(hDevice);
        return FALSE;
    }

    CloseHandle(hDevice);
    return TRUE;
}
