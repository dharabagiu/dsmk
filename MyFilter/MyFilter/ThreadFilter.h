#ifndef _THREAD_FILTER_HPP_INCLUDED_
#define _THREAD_FILTER_HPP_INCLUDED_
//

//   Author(s)    : Radu PORTASE(rportase@bitdefender.com)
//

#include "MyDriver.h"

NTSTATUS
ThreadFltInitialize();

NTSTATUS
ThreadFltUninitialize();

#endif