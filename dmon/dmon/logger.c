#include <Windows.h>
#include <assert.h>
#include <time.h>
#include <stdarg.h>
#include "logger.h"

#define MAX_MESSAGE_SIZE 4096

#define CHECK_ENABLED   \
        if (!g_Enabled) \
            return

#define START_CRITICAL_SECTION                       \
        if (WAIT_OBJECT_0 != WaitForSingleObject(g_Mutex, INFINITE)) \
            assert(FALSE)

#define END_CRITICAL_SECTION               \
        if (TRUE != ReleaseMutex(g_Mutex)) \
            assert(FALSE)

BOOL g_Enabled = FALSE;
FILE* g_File = NULL;
HANDLE g_Mutex;
int g_MinLevel = LOG_LEVEL_DEBUG;

void LoggerEnable()
{
    if (g_Enabled)
    {
        return;
    }

    if (NULL == g_File)
    {
        g_File = stdin;
    }

    g_Mutex = CreateMutex(NULL, FALSE, NULL);
    assert(NULL != g_Mutex);

    g_Enabled = TRUE;
}

void LoggerDisable()
{
    g_Enabled = FALSE;
    CloseHandle(g_Mutex);
}

void LoggerSetOutput(FILE* File)
{
    CHECK_ENABLED;
    START_CRITICAL_SECTION;

    if (NULL == File)
    {
        return;
    }
    g_File = File;

    END_CRITICAL_SECTION;
}

void LoggerSetMinLevel(int Level)
{
    CHECK_ENABLED;
    START_CRITICAL_SECTION;

    if (Level < LOG_LEVEL_DEBUG || Level > LOG_LEVEL_CRITICAL)
    {
        return;
    }
    g_MinLevel = Level;

    END_CRITICAL_SECTION;
}

void LogPrint(int Level, const char* File, int Line, const char* Format, ...)
{
    static const char* logLevelToStr[] = { "Dbg", "Inf", "Wrn", "Err", "Crt", "Ftl" };

    CHECK_ENABLED;

    if (Level < g_MinLevel || Level > LOG_LEVEL_CRITICAL)
    {
        return;
    }

    time_t timer = time(NULL);
    struct tm tmInfo;
    localtime_s(&tmInfo, &timer);

    char formattedTime[32];
    strftime(formattedTime, 32, "%Y-%m-%d %H:%M:%S", &tmInfo);

    char message[MAX_MESSAGE_SIZE];
    va_list argList;
    va_start(argList, Format);
    vsprintf_s(message, MAX_MESSAGE_SIZE, Format, argList);
    va_end(argList);

    START_CRITICAL_SECTION;

    fprintf_s(g_File, "%s|%s|%s:%d| %s\n", logLevelToStr[Level],
        formattedTime, File, Line, message);

    END_CRITICAL_SECTION;
}
