#include <Windows.h>
#include <stdlib.h>
#include <assert.h>

#include "threadpool.h"
#include "linkedlist.h"
#include "logger.h"

struct _THREAD_POOL
{
    PLINKED_LIST       WorkQueue;
    CRITICAL_SECTION   WorkQueueLock;
    CONDITION_VARIABLE WorkQueueNotEmptyCV;
    PHANDLE            Threads;
    size_t             NumberOfThreads;
    BOOL               Running;
};

typedef struct _WORK_INTERNAL
{
    WORK   Work;
    HANDLE FinishedEvent;
} WORK_INTERNAL, *PWORK_INTERNAL;

static DWORD WINAPI WorkerRoutine(LPVOID Param)
{
    PTHREAD_POOL threadPool = (PTHREAD_POOL)Param;
    if (NULL == threadPool)
    {
        LogCritical("Param cannot be NULL");
        assert(FALSE);
    }

    while (TRUE)
    {
        EnterCriticalSection(&threadPool->WorkQueueLock);

        while (0 == LLSize(threadPool->WorkQueue) && threadPool->Running)
        {
            SleepConditionVariableCS(&threadPool->WorkQueueNotEmptyCV, &threadPool->WorkQueueLock, INFINITE);
        }

        if (!threadPool->Running)
        {
            LeaveCriticalSection(&threadPool->WorkQueueLock);
            break;
        }

        WORK_INTERNAL work = *(PWORK_INTERNAL)LLGet(LLBegin(threadPool->WorkQueue));
        LLRemove(threadPool->WorkQueue, LLBegin(threadPool->WorkQueue));

        LeaveCriticalSection(&threadPool->WorkQueueLock);

        int exitStatus = work.Work.Proc(work.Work.Params);
        if (NULL != work.Work.ExitStatus)
        {
            *work.Work.ExitStatus = exitStatus;
        }
        SetEvent(work.FinishedEvent);
    }

    return 0;
}

PTHREAD_POOL ThreadPoolInit(size_t NumberOfThreads)
{
    if (0 == NumberOfThreads)
    {
        LogError("NumberOfThreads cannot be zero");
        return FALSE;
    }

    BOOL threadPoolAllocated = FALSE;
    BOOL threadHandlesAllocated = FALSE;
    BOOL workQueueInitialized = FALSE;
    BOOL workQueueNotEmptyEventCreated = FALSE;
    size_t createdThreads = 0;

    PTHREAD_POOL threadPool = (PTHREAD_POOL)malloc(sizeof(THREAD_POOL));
    if (NULL == threadPool)
    {
        LogFatal("Could not allocate memory");
        assert(FALSE);
    }
    threadPoolAllocated = TRUE;

    threadPool->Threads = (PHANDLE)malloc(sizeof(HANDLE) * NumberOfThreads);
    if (NULL == threadPool->Threads)
    {
        LogCritical("Could not allocate memory");
        assert(FALSE);
    }
    threadHandlesAllocated = TRUE;

    threadPool->WorkQueue = LLInit(sizeof(WORK_INTERNAL));
    if (NULL == threadPool->WorkQueue)
    {
        LogCritical("Could not initialize work queue");
        goto ThreadPoolInit_Failed;
    }
    workQueueInitialized = TRUE;

    InitializeCriticalSection(&threadPool->WorkQueueLock);
    InitializeConditionVariable(&threadPool->WorkQueueNotEmptyCV);

    threadPool->Running = TRUE;
    threadPool->NumberOfThreads = NumberOfThreads;

    for (size_t i = 0; i != NumberOfThreads; ++i)
    {   
        HANDLE newThread = CreateThread(NULL, 0, &WorkerRoutine, threadPool, 0, NULL);
        if (NULL == newThread)
        {
            LogCritical("Could not create thread");
            goto ThreadPoolInit_Failed;
        }
        threadPool->Threads[i] = newThread;
        ++createdThreads;
    }

    return threadPool;

ThreadPoolInit_Failed:

    for (size_t i = 0; i != createdThreads; ++i)
    {
        TerminateThread(threadPool->Threads[i], (DWORD)-1);
    }
    if (workQueueInitialized)
    {
        LLUninit(threadPool->WorkQueue);
    }
    if (threadHandlesAllocated)
    {
        free(threadPool->Threads);
    }
    if (threadPoolAllocated)
    {
        free(threadPool);
    }

    return NULL;
}

void ThreadPoolUninit(PTHREAD_POOL ThreadPool)
{
    if (NULL == ThreadPool)
    {
        LogError("ThreadPool cannot be NULL");
        return;
    }

    EnterCriticalSection(&ThreadPool->WorkQueueLock);
    ThreadPool->Running = FALSE;
    LeaveCriticalSection(&ThreadPool->WorkQueueLock);

    WakeAllConditionVariable(&ThreadPool->WorkQueueNotEmptyCV);

    if (WAIT_OBJECT_0 != WaitForMultipleObjects(ThreadPool->NumberOfThreads, ThreadPool->Threads, TRUE, INFINITE))
    {
        LogCritical("Unable to wait for thread termination");
        return;
    }

    LLUninit(ThreadPool->WorkQueue);
    free(ThreadPool->Threads);
    free(ThreadPool);
}

HANDLE ThreadPoolAddWork(PTHREAD_POOL ThreadPool, PCWORK Work)
{
    if (NULL == ThreadPool)
    {
        LogError("ThreadPool cannot be NULL");
        return NULL;
    }
    if (NULL == Work)
    {
        LogError("Work cannot be NULL");
        return NULL;
    }

    WORK_INTERNAL work;
    work.Work = *Work;
    work.FinishedEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
    if (NULL == work.FinishedEvent)
    {
        LogCritical("Cannot create event");
        return NULL;
    }

    EnterCriticalSection(&ThreadPool->WorkQueueLock);
    LLInsert(ThreadPool->WorkQueue, LLEnd(ThreadPool->WorkQueue), &work);
    LeaveCriticalSection(&ThreadPool->WorkQueueLock);

    WakeConditionVariable(&ThreadPool->WorkQueueNotEmptyCV);

    return work.FinishedEvent;
}
