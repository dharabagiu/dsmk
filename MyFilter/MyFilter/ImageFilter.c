//
//   Author(s)    : Radu PORTASE(rportase@bitdefender.com)
//
#include "ImageFilter.h"
#include "Communication.h"
#include "CommShared.h"
#include "Trace.h"
#include "ImageFilter.tmh"

static VOID
ImageFltSendMessageImageLoaded(
    _In_ HANDLE ProcessId,
    _In_ PUNICODE_STRING FullImageName
)
{
    PMY_DRIVER_IMAGE_LOADED_MSG pMsg = NULL;

    ULONG msgSize = sizeof(MY_DRIVER_IMAGE_LOADED_MSG);
    if (FullImageName)
    {
        msgSize += FullImageName->Length;
    }

    pMsg = ExAllocatePoolWithTag(PagedPool, msgSize, 'GSM+');
    if (!pMsg)
    {
        return;
    }

    pMsg->Header.MessageCode = msgImageLoaded;
    pMsg->ProcessId = HandleToULong(ProcessId);
    if (FullImageName)
    {
        pMsg->FullImageNameLength = FullImageName->Length;
        RtlCopyMemory(&pMsg->Data[0], FullImageName->Buffer, FullImageName->Length);
    }
    else
    {
        pMsg->FullImageNameLength = 0;
    }

    ULONG replySize = 0;
    NTSTATUS status = CommSendMessage(pMsg, msgSize, NULL, &replySize);
    if (!NT_SUCCESS(status))
    {
        LogError("CommSendMessage failed with status = 0x%X", status);
    }

    ExFreePoolWithTag(pMsg, 'GSM+');
}

static VOID
ImageFltNotifyRoutine(
    _In_ PUNICODE_STRING FullImageName,
    _In_ HANDLE ProcessId,
    _In_ PIMAGE_INFO ImageInfo
)
{
    UNREFERENCED_PARAMETER(ImageInfo);

    ImageFltSendMessageImageLoaded(ProcessId, FullImageName);
}

NTSTATUS
ImageFltInitialize()
{
    return PsSetLoadImageNotifyRoutine(ImageFltNotifyRoutine);
}

NTSTATUS
ImageFltUninitialize()
{
    return PsRemoveLoadImageNotifyRoutine(ImageFltNotifyRoutine);
}
