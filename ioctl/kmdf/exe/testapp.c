#include <DriverSpecs.h>
_Analysis_mode_(_Analysis_code_type_user_code_)  

#include <windows.h>

#pragma warning(disable:4201)  // nameless struct/union
#include <winioctl.h>
#pragma warning(default:4201)

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <limits.h>
#include <strsafe.h>
#include "public.h"


VOID
DoIoctls(
    HANDLE hDevice
    );

DWORD WINAPI MyThread(
    _In_ LPVOID lpParameter
);


typedef struct _MY_THREAD_PARAM {
    HANDLE hDevice;
    OVERLAPPED* pOverlapped;
    char* outputBuffer;
} MY_THREAD_PARAM;


//-----------------------------------------------------------------------------
// 4127 -- Conditional Expression is Constant warning
//-----------------------------------------------------------------------------
#define WHILE(constant) \
__pragma(warning(disable: 4127)) while(constant); __pragma(warning(default: 4127))



VOID __cdecl
main()
{
    HANDLE   hDevice;

    hDevice = CreateFile(DEVICE_NAME,
                         GENERIC_READ | GENERIC_WRITE,
                         FILE_SHARE_READ | FILE_SHARE_WRITE,
                         NULL,
                         CREATE_ALWAYS,
                         FILE_ATTRIBUTE_NORMAL | FILE_FLAG_OVERLAPPED,
                         NULL);

    if(hDevice == INVALID_HANDLE_VALUE) {
        printf("Cannot open device\n");
        return;
    }

    DoIoctls(hDevice);

    CloseHandle ( hDevice );

    return;
}


VOID
DoIoctls(
    HANDLE hDevice
    )
{
    char OutputBuffer[1000];
    char InputBuffer[200];
    BOOL bRc;
    ULONG bytesReturned;
    ULONG bytesReturned2;

    // Sending REVERSE IOCTL

    memset(OutputBuffer, 0, sizeof(OutputBuffer));


    OVERLAPPED overlapped;
    ZeroMemory(&overlapped, sizeof(overlapped));
    overlapped.hEvent = CreateEvent(NULL, FALSE, FALSE, TEXT("TestEvent666"));

    bRc = DeviceIoControl(hDevice,
        (DWORD)IOCTL_NONPNP_REVERSE,
        InputBuffer,
        (DWORD)strlen(InputBuffer) + 1,
        OutputBuffer,
        sizeof(OutputBuffer),
        &bytesReturned,
        &overlapped
    );

    if (!bRc && GetLastError() != ERROR_IO_PENDING)
    {
        printf("Error in DeviceIoControl2 : %d\n", GetLastError());
        return;
    }

    MY_THREAD_PARAM threadParam;
    threadParam.hDevice = hDevice;
    threadParam.pOverlapped = &overlapped;
    threadParam.outputBuffer = OutputBuffer;
    HANDLE hThread = CreateThread(NULL, 0, &MyThread, &threadParam, 0, NULL);

    // Sending DIRECT IOCTL

    if(FAILED(StringCchCopy(InputBuffer, sizeof(InputBuffer),
        "sorina-cu_norocul_sunt_frate_de_cruce-ro-dvdrip-x264-___wWw.VitanClub.Net___.mkv"))){
        return;
    }

    OVERLAPPED overlapped2;
    ZeroMemory(&overlapped2, sizeof(overlapped2));
    overlapped2.hEvent = CreateEvent(NULL, FALSE, FALSE, TEXT("TestEvent667"));

    bRc = DeviceIoControl ( hDevice,
                            (DWORD) IOCTL_NONPNP_DIRECT,
                            InputBuffer,
                            (DWORD) strlen( InputBuffer )+1,
                            OutputBuffer,
                            sizeof(OutputBuffer),
                            &bytesReturned2,
                            &overlapped2
                            );

    if ( !bRc && GetLastError() != ERROR_IO_PENDING )
    {
        printf ( "Error in DeviceIoControl1 : %d", GetLastError());
        return;
    }

    GetOverlappedResult(hDevice, &overlapped2, &bytesReturned2, TRUE);

    WaitForSingleObject(hThread, INFINITE);

    CloseHandle(overlapped.hEvent);
    CloseHandle(hThread);

    return;

}

DWORD WINAPI MyThread(
    _In_ LPVOID lpParameter
)
{
    MY_THREAD_PARAM* param = (MY_THREAD_PARAM*)lpParameter;
    
    DWORD bytesReturned;
    GetOverlappedResult(param->hDevice, param->pOverlapped, &bytesReturned, TRUE);

    printf("OutBuffer (%d): %s\n", bytesReturned, param->outputBuffer);

    return 0;
}
