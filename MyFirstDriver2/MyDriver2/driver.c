#include <wdm.h>

#define DbgInfo(format, ...) \
	DbgPrintEx(DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, (format), __VA_ARGS__);
#define DbgError(format, ...) \
	DbgPrintEx(DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, (format), __VA_ARGS__);

#define NT_DEVICE_NAME	L"\\Device\\Driver2"

#define IOCTL_CUSTOM_1 CTL_CODE(FILE_DEVICE_UNKNOWN, 0xd80, METHOD_NEITHER, FILE_ANY_ACCESS)
#define IOCTL_CUSTOM_2 CTL_CODE(FILE_DEVICE_UNKNOWN, 0xd81, METHOD_NEITHER, FILE_ANY_ACCESS)

DRIVER_INITIALIZE DriverEntry;
DRIVER_UNLOAD	  DriverUnload;
DRIVER_DISPATCH   CbIoctlCreateClose;
DRIVER_DISPATCH   CbIoctlDeviceControl;

void
DriverUnload(
	_In_ PDRIVER_OBJECT DriverObject
)
{
	PDEVICE_OBJECT deviceObject = DriverObject->DeviceObject;

	PAGED_CODE();

	if (deviceObject != NULL)
	{
		IoDeleteDevice(deviceObject);
	}
}

NTSTATUS
CbIoctlCreateClose(
	_In_ PDEVICE_OBJECT DeviceObject,
	_In_ PIRP Irp
)
{
	UNREFERENCED_PARAMETER(DeviceObject);

	PAGED_CODE();

	Irp->IoStatus.Status = STATUS_SUCCESS;
	Irp->IoStatus.Information = 0;

	IoCompleteRequest(Irp, IO_NO_INCREMENT);

	return STATUS_SUCCESS;
}

NTSTATUS
CbIoctlDeviceControl(
	_In_    PDEVICE_OBJECT DeviceObject,
	_Inout_ PIRP		   Irp
)
{
	PIO_STACK_LOCATION irpSp;
	NTSTATUS		   ntStatus = STATUS_SUCCESS;
	ULONG			   ioControlCode;

	UNREFERENCED_PARAMETER(DeviceObject);

	PAGED_CODE();

	irpSp = IoGetCurrentIrpStackLocation(Irp);
	ioControlCode = irpSp->Parameters.DeviceIoControl.IoControlCode;

	switch (ioControlCode)
	{
		case IOCTL_CUSTOM_1:
		{
			DbgInfo("First IOCTL was called");
			__debugbreak();
			break;
		}
		case IOCTL_CUSTOM_2:
		{
			DbgInfo("Second IOCTL was called");
			__debugbreak();
			break;
		}
		default:
		{
			break;
		}
	}

	IoCompleteRequest(Irp, IO_NO_INCREMENT);

	return ntStatus;
}

NTSTATUS
DriverEntry(
	_In_ PDRIVER_OBJECT	 DriverObject,
	_In_ PUNICODE_STRING RegistryPath
)
{
	NTSTATUS       ntStatus = STATUS_SUCCESS;
	UNICODE_STRING ntDeviceName = { 0 };
	PDEVICE_OBJECT deviceObject = NULL;

	UNREFERENCED_PARAMETER(RegistryPath);

	RtlInitUnicodeString(&ntDeviceName, NT_DEVICE_NAME);

	ntStatus = IoCreateDevice(
		DriverObject,
		0,
		&ntDeviceName,
		FILE_DEVICE_UNKNOWN,
		FILE_DEVICE_SECURE_OPEN,
		FALSE,
		&deviceObject);
	if (!NT_SUCCESS(ntStatus))
	{
		DbgError("IoCreateDevice error: 0x%lx", ntStatus);
		return ntStatus;
	}

	DriverObject->DriverUnload = DriverUnload;
	DriverObject->MajorFunction[IRP_MJ_CREATE] = CbIoctlCreateClose;
	DriverObject->MajorFunction[IRP_MJ_CLOSE] = CbIoctlCreateClose;
	DriverObject->MajorFunction[IRP_MJ_DEVICE_CONTROL] = CbIoctlDeviceControl;

	return ntStatus;
}
