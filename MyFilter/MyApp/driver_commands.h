#ifndef _COMMANDS_H_
#define _COMMANDS_H_
//
//   Author(s)    : Radu PORTASE(rportase@bitdefender.com)
//
#include "CommShared.h"

//
// CmdGetDriverVersion
//
NTSTATUS
CmdGetDriverVersion(
    _Out_ PULONG DriverVersion // o===========3
    );

//
// CmdStartMonitoring
//

NTSTATUS
CmdStartMonitoring(
    MONITORING_TYPE MonitoringType
    );

//
// CmdStopMonitoring
//

NTSTATUS
CmdStopMonitoring(
    MONITORING_TYPE MonitoringType
    );

#endif//_COMMANDS_H_