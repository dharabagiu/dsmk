// Din ceva motiv primesc BSOD si crapa, logica pare buna cred
// Sincronizarea sper ca e buna, work-urile sunt distribuite egal dupa intuitia mea
// Nu e cel mai frumos cod dar l-am facut pe fuga

#include <fltKernel.h>

#define MyDbgPrint(format, ...) \
	DbgPrintEx(DPFLTR_SYSTEM_ID, DPFLTR_ERROR_LEVEL, (format), __VA_ARGS__);

DRIVER_INITIALIZE DriverEntry;
DRIVER_UNLOAD DriverUnload;

typedef struct
{
	void(*Function)(void*);
	void* Parameters;
	LIST_ENTRY ListEntry;
} WORK;

typedef struct
{
	int ThreadIndex;
	LIST_ENTRY ListEntry;
} WAITING_THREAD;

HANDLE* Workers;
int* WorkersContext;
LIST_ENTRY WorkQueueHead;
BOOLEAN ThreadPoolRunning;
int ThreadPoolThreadCount;
KSPIN_LOCK WorkQueueLock;
KIRQL OldIrql;

// Aici am incercat sa imit un condition variable, care are o coada de thread-uri in asteptare
// Ca sa rezolv problema cu distribuirea egala a work-urilor intre thread-uri
LIST_ENTRY WaitingThreadsQueueHead;
KEVENT* ThreadWakeEvents;

void WorkerRoutine(PVOID StartContext)
{
	int threadIndex = *(int*)StartContext;

	while (TRUE)
	{
		KeAcquireSpinLock(&WorkQueueLock, &OldIrql);

		while (IsListEmpty(&WorkQueueHead))
		{
			if (!ThreadPoolRunning)
			{
				KeReleaseSpinLock(&WorkQueueLock, OldIrql);
				break;
			}

			WAITING_THREAD* wt = (WAITING_THREAD*)ExAllocatePoolWithTag(PagedPool, sizeof(WAITING_THREAD), '7gaT');
			wt->ThreadIndex = threadIndex;
			InsertTailList(&WaitingThreadsQueueHead, &wt->ListEntry);

			KeReleaseSpinLock(&WorkQueueLock, OldIrql);

			KeWaitForSingleObject(&ThreadWakeEvents[threadIndex], Executive, KernelMode, FALSE, NULL);
			KeAcquireSpinLock(&WorkQueueLock, &OldIrql);
		}

		PLIST_ENTRY listEntry = RemoveHeadList(&WorkQueueHead);
		KeReleaseSpinLock(&WorkQueueLock, OldIrql);

		WORK* work = CONTAINING_RECORD(listEntry, WORK, ListEntry);
		work->Function(work->Parameters);

		ExFreePool(work);
	}

	PsTerminateSystemThread(0);
}

void CreateThreadPool(int NumberOfThreads)
{
	UNREFERENCED_PARAMETER(NumberOfThreads);

	InitializeListHead(&WorkQueueHead);
	ThreadPoolRunning = TRUE;
	ThreadPoolThreadCount = NumberOfThreads;

	OBJECT_ATTRIBUTES attributes;
	InitializeObjectAttributes(
		&attributes,
		NULL,
		OBJ_KERNEL_HANDLE,
		NULL,
		NULL);
	Workers = (HANDLE*)ExAllocatePoolWithTag(
		PagedPool,
		sizeof(HANDLE) * NumberOfThreads,
		'2gaT');
	WorkersContext = (int*)ExAllocatePoolWithTag(
		PagedPool,
		sizeof(int) * NumberOfThreads,
		'9gaT');
	ThreadWakeEvents = (KEVENT*)ExAllocatePoolWithTag(
		PagedPool,
		sizeof(KEVENT) * NumberOfThreads,
		'8gaT');
	KeInitializeSpinLock(&WorkQueueLock);
	for (int i = 0; i != NumberOfThreads; ++i)
	{
		KeInitializeEvent(&ThreadWakeEvents[i], SynchronizationEvent, FALSE);
		WorkersContext[i] = i;
		PsCreateSystemThread(
			&Workers[i],
			DELETE | SYNCHRONIZE,
			&attributes,
			NULL,
			NULL,
			&WorkerRoutine,
			&WorkersContext[i]);
	}
}

void DestroyThreadPool()
{
	KeAcquireSpinLock(&WorkQueueLock, &OldIrql);
	ThreadPoolRunning = FALSE;
	while (!IsListEmpty(&WaitingThreadsQueueHead))
	{
		PLIST_ENTRY listEntry = RemoveHeadList(&WaitingThreadsQueueHead);
		WAITING_THREAD* wt = CONTAINING_RECORD(listEntry, WAITING_THREAD, ListEntry);
		KeSetEvent(&ThreadWakeEvents[wt->ThreadIndex], 0, FALSE);
		ExFreePool(wt);
	}
	KeReleaseSpinLock(&WorkQueueLock, OldIrql);

	for (int i = 0; i != ThreadPoolThreadCount; ++i)
	{
		ZwWaitForSingleObject(Workers[i], FALSE, NULL);
		ZwClose(Workers[i]);
	}

	ExFreePool(Workers);
	ExFreePool(WorkersContext);
	ExFreePool(ThreadWakeEvents);

	PLIST_ENTRY iter = WorkQueueHead.Flink;
	while (iter != &WorkQueueHead)
	{
		PLIST_ENTRY next = iter->Flink;
		WORK* work = CONTAINING_RECORD(iter, WORK, ListEntry);
		ExFreePool(work);
		iter = next;
	}
}

void AddWork(void(*Function)(void*), void* Parameters)
{
	WORK* work = (WORK*)ExAllocatePoolWithTag(PagedPool, sizeof(WORK), '1gaT');
	work->Function = Function;
	work->Parameters = Parameters;

	KeAcquireSpinLock(&WorkQueueLock, &OldIrql);
	InsertTailList(&WorkQueueHead, &work->ListEntry);

	if (!IsListEmpty(&WaitingThreadsQueueHead))
	{
		PLIST_ENTRY listEntry = RemoveHeadList(&WaitingThreadsQueueHead);
		WAITING_THREAD* wt = CONTAINING_RECORD(listEntry, WAITING_THREAD, ListEntry);
		KeSetEvent(&ThreadWakeEvents[wt->ThreadIndex], 0, FALSE);
		ExFreePool(wt);
	}
	
	KeReleaseSpinLock(&WorkQueueLock, OldIrql);
}

void
DriverUnload(
	_In_ PDRIVER_OBJECT DriverObject
)
{
	UNREFERENCED_PARAMETER(DriverObject);

	PAGED_CODE();
}

void TestThreadPool_Work(void* Params)
{
	*(BOOLEAN*)Params = TRUE;
}

void TestThreadPool()
{
	CreateThreadPool(10);

	BOOLEAN completed[10000];

	for (int i = 0; i < 10000; ++i)
	{
		completed[i] = FALSE;
		AddWork(&TestThreadPool_Work, &completed[i]);
	}

	DestroyThreadPool();

	for (int i = 0; i < 10000; ++i)
	{
		if (!completed[i])
		{
			MyDbgPrint("Doesn't work");
			__debugbreak();
			return;
		}
	}

	MyDbgPrint("WORKS!!");
	__debugbreak();
}

NTSTATUS
DriverEntry(
	_In_ PDRIVER_OBJECT	 DriverObject,
	_In_ PUNICODE_STRING RegistryPath
)
{
	NTSTATUS       ntStatus = STATUS_SUCCESS;

	UNREFERENCED_PARAMETER(RegistryPath);

	DriverObject->DriverUnload = DriverUnload;

	TestThreadPool();

	return ntStatus;
}
