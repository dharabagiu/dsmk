#include <wdm.h>

#define DbgInfo(format, ...) \
	DbgPrintEx(DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, (format), __VA_ARGS__);
#define DbgError(format, ...) \
	DbgPrintEx(DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, (format), __VA_ARGS__);

#define NT_DEVICE_NAME	L"\\Device\\Driver1"
#define DOS_DEVICE_NAME L"\\DosDevices\\Driver1"

DRIVER_INITIALIZE DriverEntry;
DRIVER_UNLOAD	  DriverUnload;
DRIVER_DISPATCH   CbIoctlCreateClose;
DRIVER_DISPATCH   CbIoctlDeviceControl;

PFILE_OBJECT   DestinationFileObject;
PDEVICE_OBJECT DestinationDeviceObject;

void
DriverUnload(
	_In_ PDRIVER_OBJECT DriverObject
)
{
	PDEVICE_OBJECT deviceObject = DriverObject->DeviceObject;
	UNICODE_STRING dosDeviceName;

	PAGED_CODE();

	ObDereferenceObject(DestinationFileObject);

	RtlInitUnicodeString(&dosDeviceName, DOS_DEVICE_NAME);

	IoDeleteSymbolicLink(&dosDeviceName);
	if (deviceObject != NULL)
	{
		IoDeleteDevice(deviceObject);
	}
}

NTSTATUS
CbIoctlCreateClose(
	_In_ PDEVICE_OBJECT DeviceObject,
	_In_ PIRP Irp
)
{
	UNREFERENCED_PARAMETER(DeviceObject);

	PAGED_CODE();

	Irp->IoStatus.Status = STATUS_SUCCESS;
	Irp->IoStatus.Information = 0;

	IoCompleteRequest(Irp, IO_NO_INCREMENT);

	return STATUS_SUCCESS;
}

NTSTATUS
MyCompletionRoutine(
	PDEVICE_OBJECT DeviceObject,
	PIRP		   Irp,
	PVOID          Context
)
{
	NTSTATUS ntStatus;

	UNREFERENCED_PARAMETER(DeviceObject);
	UNREFERENCED_PARAMETER(Context);

	ntStatus = Irp->IoStatus.Status;;
	DbgInfo("IRP completion status: 0x%lx", ntStatus);
	__debugbreak();

	return STATUS_SUCCESS;
}

NTSTATUS
CbIoctlDeviceControl(
	_In_    PDEVICE_OBJECT DeviceObject,
	_Inout_ PIRP		   Irp
)
{
	NTSTATUS		   ntStatus = STATUS_SUCCESS;

	UNREFERENCED_PARAMETER(DeviceObject);

	PAGED_CODE();

	IoCopyCurrentIrpStackLocationToNext(Irp);

	IoSetCompletionRoutine(
		Irp,
		&MyCompletionRoutine,
		NULL,
		TRUE,
		TRUE,
		TRUE
	);

	ntStatus = IoCallDriver(DestinationDeviceObject, Irp);
	if (!NT_SUCCESS(ntStatus))
	{
		DbgError("IoCallDriver error: 0x%lx", ntStatus);
		return ntStatus;
	}

	return ntStatus;
}

NTSTATUS
DriverEntry(
	_In_ PDRIVER_OBJECT	 DriverObject,
	_In_ PUNICODE_STRING RegistryPath
)
{
	NTSTATUS       ntStatus = STATUS_SUCCESS;
	UNICODE_STRING ntDeviceName = { 0 };
	UNICODE_STRING dosDeviceName = { 0 };
	PDEVICE_OBJECT deviceObject = NULL;
	UNICODE_STRING destinationDeviceName;

	UNREFERENCED_PARAMETER(RegistryPath);

	RtlInitUnicodeString(&ntDeviceName, NT_DEVICE_NAME);

	ntStatus = IoCreateDevice(
		DriverObject,
		0,
		&ntDeviceName,
		FILE_DEVICE_UNKNOWN,
		FILE_DEVICE_SECURE_OPEN,
		FALSE,
		&deviceObject);
	if (!NT_SUCCESS(ntStatus))
	{
		DbgError("IoCreateDevice error: 0x%lx", ntStatus);
		return ntStatus;
	}

	deviceObject->StackSize = 2;

	DriverObject->DriverUnload = DriverUnload;
	DriverObject->MajorFunction[IRP_MJ_CREATE] = CbIoctlCreateClose;
	DriverObject->MajorFunction[IRP_MJ_CLOSE] = CbIoctlCreateClose;
	DriverObject->MajorFunction[IRP_MJ_DEVICE_CONTROL] = CbIoctlDeviceControl;

	RtlInitUnicodeString(&dosDeviceName, DOS_DEVICE_NAME);

	ntStatus = IoCreateSymbolicLink(&dosDeviceName, &ntDeviceName);
	if (!NT_SUCCESS(ntStatus))
	{
		DbgError("IoCreateSymbolicLink error: 0x%lx", ntStatus);
		IoDeleteDevice(deviceObject);
		return ntStatus;
	}

	RtlInitUnicodeString(&destinationDeviceName, L"\\Device\\Driver2");
	ntStatus = IoGetDeviceObjectPointer(
		&destinationDeviceName,
		FILE_ALL_ACCESS,
		&DestinationFileObject,
		&DestinationDeviceObject);
	if (!NT_SUCCESS(ntStatus))
	{
		DbgError("IoGetDeviceObjectPointer error: 0x%lx", ntStatus);
		return ntStatus;
	}

	return ntStatus;
}
