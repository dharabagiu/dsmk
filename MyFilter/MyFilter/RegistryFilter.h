#ifndef _REGISTRY_FILTER_HPP_INCLUDED_
#define _REGISTRY_FILTER_HPP_INCLUDED_
//

//   Author(s)    : Radu PORTASE(rportase@bitdefender.com)
//

#include "MyDriver.h"

NTSTATUS
RegistryFltInitialize();

NTSTATUS
RegistryFltUninitialize();

#endif