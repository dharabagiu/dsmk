//
//   Author(s)    : Radu PORTASE(rportase@bitdefender.com)
//

#include "messagehandlers.h"
#include <ntstatus.h>
#include "globaldata.h"
#include "CommShared.h"
#include <malloc.h>

//
// MsgHandleUnknownMessage
//
_Pre_satisfies_(InputBufferSize >= sizeof(FILTER_MESSAGE_HEADER))
_Pre_satisfies_(OutputBufferSize >= sizeof(FILTER_REPLY_HEADER))
NTSTATUS
MsgHandleUnknownMessage(
    _In_bytecount_(InputBufferSize) PFILTER_MESSAGE_HEADER InputBuffer,
    _In_ DWORD InputBufferSize,
    _Out_writes_bytes_to_opt_(OutputBufferSize, *BytesWritten) PFILTER_REPLY_HEADER OutputBuffer,
    _In_ DWORD OutputBufferSize,
    _Out_ PDWORD BytesWritten
)
{
    UNREFERENCED_PARAMETER(InputBufferSize);
    UNREFERENCED_PARAMETER(OutputBuffer);
    UNREFERENCED_PARAMETER(OutputBufferSize);
    PMY_DRIVER_MESSAGE_HEADER pHeader = (PMY_DRIVER_MESSAGE_HEADER)(InputBuffer + 1);

    wprintf(L"[Error] Unknown message received form driver. Id = %u", pHeader->MessageCode);

    *BytesWritten = 0;
    return STATUS_SUCCESS;
}

//
// MsgHandleProcessCreate
//
_Pre_satisfies_(InputBufferSize >= sizeof(FILTER_MESSAGE_HEADER))
_Pre_satisfies_(OutputBufferSize >= sizeof(FILTER_REPLY_HEADER))
NTSTATUS
MsgHandleProcessCreate(
    _In_bytecount_(InputBufferSize) PFILTER_MESSAGE_HEADER InputBuffer,
    _In_ DWORD InputBufferSize,
    _Out_writes_bytes_to_opt_(OutputBufferSize, *BytesWritten) PFILTER_REPLY_HEADER OutputBuffer,
    _In_ DWORD OutputBufferSize,
    _Out_ PDWORD BytesWritten
)
{
    PMY_DRIVER_PROCESS_NOTIFICATION_FULL_MESSAGE pInput = (PMY_DRIVER_PROCESS_NOTIFICATION_FULL_MESSAGE)InputBuffer;
    PMY_DRIVER_PROCESS_CREATE_FULL_MESSAGE_REPLY  pOutput = (PMY_DRIVER_PROCESS_CREATE_FULL_MESSAGE_REPLY)OutputBuffer;

    *BytesWritten = 0;
    if (InputBufferSize < sizeof(*pInput))
    {
        return STATUS_INVALID_USER_BUFFER;
    }

    if (OutputBufferSize < sizeof(*pOutput))
    {
        return STATUS_INVALID_USER_BUFFER;
    }

    if (sizeof(*pInput) + pInput->Message.ImagePathLength < sizeof(*pInput))
    {
        return STATUS_INTEGER_OVERFLOW;
    }

    if (InputBufferSize < sizeof(*pInput) + pInput->Message.ImagePathLength)
    {
        return STATUS_INVALID_USER_BUFFER;
    }

    *BytesWritten = sizeof(*pOutput);
    pOutput->Reply.Status = STATUS_SUCCESS;

    PWCHAR path = malloc(pInput->Message.ImagePathLength + sizeof(WCHAR));
    memcpy(path, &pInput->Message.Data[0], pInput->Message.ImagePathLength);
    path[pInput->Message.ImagePathLength >> 1] = L'\0';

    PWCHAR commandLine = malloc(pInput->Message.ProcessCommandLineLength + sizeof(WCHAR));
    memcpy(path, &pInput->Message.Data[0] + pInput->Message.ImagePathLength, pInput->Message.ProcessCommandLineLength);
    commandLine[pInput->Message.ProcessCommandLineLength >> 1] = L'\0';

    wprintf(L"[PROC] Process Created: id = %d, path = %s, commandLine = %s\n", pInput->Message.ProcessId, path, commandLine);
    free(path);
    free(commandLine);

    return STATUS_SUCCESS;
}

_Pre_satisfies_(InputBufferSize >= sizeof(FILTER_MESSAGE_HEADER))
_Pre_satisfies_(OutputBufferSize >= sizeof(FILTER_REPLY_HEADER))
NTSTATUS
MsgHandleProcessTerminate(
    _In_bytecount_(InputBufferSize) PFILTER_MESSAGE_HEADER InputBuffer,
    _In_  DWORD InputBufferSize,
    _Out_writes_bytes_to_opt_(OutputBufferSize, *BytesWritten) PFILTER_REPLY_HEADER OutputBuffer,
    _In_  DWORD OutputBufferSize,
    _Out_ PDWORD BytesWritten
)
{
	UNREFERENCED_PARAMETER(OutputBuffer);
	UNREFERENCED_PARAMETER(OutputBufferSize);

    PMY_DRIVER_PROCESS_TERMINATE_MSG_FULL msg = (PMY_DRIVER_PROCESS_TERMINATE_MSG_FULL)InputBuffer;

    if (!InputBuffer || InputBufferSize < sizeof(*msg))
    {
        return STATUS_INVALID_USER_BUFFER;
    }

    wprintf(L"[PROC] Pid %u terminates. \n", msg->Msg.ProcessId);

    *BytesWritten = 0;
	return STATUS_SUCCESS;
}

NTSTATUS
MsgHandleThreadCreate(
    _In_bytecount_(InputBufferSize) PFILTER_MESSAGE_HEADER InputBuffer,
    _In_  DWORD InputBufferSize,
    _Out_writes_bytes_to_opt_(OutputBufferSize, *BytesWritten) PFILTER_REPLY_HEADER OutputBuffer,
    _In_  DWORD OutputBufferSize,
    _Out_ PDWORD BytesWritten
)
{
    UNREFERENCED_PARAMETER(OutputBuffer);
    UNREFERENCED_PARAMETER(OutputBufferSize);

    PMY_DRIVER_THREAD_MSG_FULL msg = (PMY_DRIVER_THREAD_MSG_FULL)InputBuffer;

    if (!InputBuffer || InputBufferSize < sizeof(*msg))
    {
        return STATUS_INVALID_USER_BUFFER;
    }

    wprintf(L"[THREAD] Pid %u Tid %u created. \n", msg->Message.ProcessId, msg->Message.ThreadId);

    *BytesWritten = 0;
    return STATUS_SUCCESS;
}

NTSTATUS
MsgHandleThreadTerminate(
    _In_bytecount_(InputBufferSize) PFILTER_MESSAGE_HEADER InputBuffer,
    _In_  DWORD InputBufferSize,
    _Out_writes_bytes_to_opt_(OutputBufferSize, *BytesWritten) PFILTER_REPLY_HEADER OutputBuffer,
    _In_  DWORD OutputBufferSize,
    _Out_ PDWORD BytesWritten
)
{
    UNREFERENCED_PARAMETER(OutputBuffer);
    UNREFERENCED_PARAMETER(OutputBufferSize);

    PMY_DRIVER_THREAD_MSG_FULL msg = (PMY_DRIVER_THREAD_MSG_FULL)InputBuffer;

    if (!InputBuffer || InputBufferSize < sizeof(*msg))
    {
        return STATUS_INVALID_USER_BUFFER;
    }

    wprintf(L"[THREAD] Pid %u Tid %u terminated. \n", msg->Message.ProcessId, msg->Message.ThreadId);

    *BytesWritten = 0;
    return STATUS_SUCCESS;
}

_Pre_satisfies_(InputBufferSize >= sizeof(FILTER_MESSAGE_HEADER))
_Pre_satisfies_(OutputBufferSize >= sizeof(FILTER_REPLY_HEADER))
NTSTATUS
MsgHandleImageLoad(
    _In_bytecount_(InputBufferSize) PFILTER_MESSAGE_HEADER InputBuffer,
    _In_ DWORD InputBufferSize,
    _Out_writes_bytes_to_opt_(OutputBufferSize, *BytesWritten) PFILTER_REPLY_HEADER OutputBuffer,
    _In_ DWORD OutputBufferSize,
    _Out_ PDWORD BytesWritten
)
{
    UNREFERENCED_PARAMETER(InputBufferSize);
    UNREFERENCED_PARAMETER(OutputBuffer);
    UNREFERENCED_PARAMETER(OutputBufferSize);

    PMY_DRIVER_IMAGE_LOADED_MSG_FULL pInput = (PMY_DRIVER_IMAGE_LOADED_MSG_FULL)InputBuffer;

    *BytesWritten = 0;

    PWCHAR path = malloc(pInput->Message.FullImageNameLength + sizeof(WCHAR));
    memcpy(path, &pInput->Message.Data[0], pInput->Message.FullImageNameLength);
    path[pInput->Message.FullImageNameLength >> 1] = L'\0';

    wprintf(L"[PROC] Image loaded: pid = %u, image = %s\n", pInput->Message.ProcessId, path);
    free(path);

    return STATUS_SUCCESS;
}

_Pre_satisfies_(InputBufferSize >= sizeof(FILTER_MESSAGE_HEADER))
_Pre_satisfies_(OutputBufferSize >= sizeof(FILTER_REPLY_HEADER))
NTSTATUS
MsgHandleRegistryOperation(
    _In_bytecount_(InputBufferSize) PFILTER_MESSAGE_HEADER InputBuffer,
    _In_ DWORD InputBufferSize,
    _Out_writes_bytes_to_opt_(OutputBufferSize, *BytesWritten) PFILTER_REPLY_HEADER OutputBuffer,
    _In_ DWORD OutputBufferSize,
    _Out_ PDWORD BytesWritten
)
{
    UNREFERENCED_PARAMETER(InputBufferSize);
    UNREFERENCED_PARAMETER(OutputBuffer);
    UNREFERENCED_PARAMETER(OutputBufferSize);

    PMY_DRIVER_REGISTRY_OPERATION_MSG_FULL pInput = (PMY_DRIVER_REGISTRY_OPERATION_MSG_FULL)InputBuffer;

    *BytesWritten = 0;

    PWCHAR description = malloc(pInput->Message.OperationDescriptionLength + sizeof(WCHAR));
    memcpy(description, &pInput->Message.Data[0], pInput->Message.OperationDescriptionLength);
    description[pInput->Message.OperationDescriptionLength >> 1] = L'\0';

    wprintf(L"[PROC] Registry operation: %s\n", description);
    free(description);

    return STATUS_SUCCESS;
}

_Pre_satisfies_(InputBufferSize >= sizeof(FILTER_MESSAGE_HEADER))
_Pre_satisfies_(OutputBufferSize >= sizeof(FILTER_REPLY_HEADER))
NTSTATUS
MsgHandleFileOperation(
    _In_bytecount_(InputBufferSize) PFILTER_MESSAGE_HEADER InputBuffer,
    _In_ DWORD InputBufferSize,
    _Out_writes_bytes_to_opt_(OutputBufferSize, *BytesWritten) PFILTER_REPLY_HEADER OutputBuffer,
    _In_ DWORD OutputBufferSize,
    _Out_ PDWORD BytesWritten
)
{
    UNREFERENCED_PARAMETER(InputBufferSize);
    UNREFERENCED_PARAMETER(OutputBuffer);
    UNREFERENCED_PARAMETER(OutputBufferSize);

    PMY_DRIVER_FILE_OPERATION_MSG_FULL pInput = (PMY_DRIVER_FILE_OPERATION_MSG_FULL)InputBuffer;

    *BytesWritten = 0;

    PWCHAR description = malloc(pInput->Message.OperationDescriptionLength + sizeof(WCHAR));
    memcpy(description, &pInput->Message.Data[0], pInput->Message.OperationDescriptionLength);
    description[pInput->Message.OperationDescriptionLength >> 1] = L'\0';

    wprintf(L"[PROC] File operation: %s\n", description);
    free(description);

    return STATUS_SUCCESS;
}

//
// MsgDispatchNewMessage
//
_Pre_satisfies_(InputBufferSize >= sizeof(FILTER_MESSAGE_HEADER))
_Pre_satisfies_(OutputBufferSize >= sizeof(FILTER_REPLY_HEADER))
NTSTATUS
MsgDispatchNewMessage(
    _In_bytecount_(InputBufferSize) PFILTER_MESSAGE_HEADER InputBuffer,
    _In_ DWORD InputBufferSize,
    _Out_writes_bytes_to_opt_(OutputBufferSize, *BytesWritten) PFILTER_REPLY_HEADER OutputBuffer,
    _In_ DWORD OutputBufferSize,
    _Out_ PDWORD BytesWritten
    )
{
    NTSTATUS status = STATUS_UNSUCCESSFUL;
    if (InputBufferSize < sizeof(FILTER_MESSAGE_HEADER) + sizeof(MY_DRIVER_COMMAND_HEADER))
    {
        wprintf(L"[Error] Message size is too small to dispatch. Size = %d\n", InputBufferSize);
        return STATUS_BUFFER_TOO_SMALL;
    }

    PMY_DRIVER_MESSAGE_HEADER pHeader = (PMY_DRIVER_MESSAGE_HEADER)(InputBuffer + 1);

    switch (pHeader->MessageCode)
    {
    case msgProcessCreate:
        status = MsgHandleProcessCreate(InputBuffer, InputBufferSize, OutputBuffer, OutputBufferSize, BytesWritten);
        break;
    case msgProcessTerminate:
        status = MsgHandleProcessTerminate(InputBuffer, InputBufferSize, OutputBuffer, OutputBufferSize, BytesWritten);
        break;
    case msgThreadCreate:
        status = MsgHandleThreadCreate(InputBuffer, InputBufferSize, OutputBuffer, OutputBufferSize, BytesWritten);
        break;
    case msgThreadTerminate:
        status = MsgHandleThreadTerminate(InputBuffer, InputBufferSize, OutputBuffer, OutputBufferSize, BytesWritten);
        break;
    case msgImageLoaded:
        status = MsgHandleImageLoad(InputBuffer, InputBufferSize, OutputBuffer, OutputBufferSize, BytesWritten);
        break;
    case msgRegistryOperation:
        status = MsgHandleRegistryOperation(InputBuffer, InputBufferSize, OutputBuffer, OutputBufferSize, BytesWritten);
        break;
    case msgFileOperation:
        status = MsgHandleFileOperation(InputBuffer, InputBufferSize, OutputBuffer, OutputBufferSize, BytesWritten);
        break;
    default:
        status =  MsgHandleUnknownMessage(InputBuffer, InputBufferSize, OutputBuffer, OutputBufferSize, BytesWritten);
        break;
    }
    
    return status;
}
