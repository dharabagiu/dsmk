#include <fltKernel.h>

#define MyDbgPrint(format, ...) \
	DbgPrintEx(DPFLTR_SYSTEM_ID, DPFLTR_ERROR_LEVEL, (format), __VA_ARGS__);

#define NT_DEVICE_NAME	L"\\Device\\DmonDriver"
#define DOS_DEVICE_NAME L"\\DosDevices\\DmonDriver"
#define MAX_IMAGE_NAME 4096
#define PROCESS_TERMINATE 1
#define IOCTL_PROTECT_PROCESS CTL_CODE(FILE_DEVICE_UNKNOWN, 0xd80, METHOD_BUFFERED, FILE_ANY_ACCESS)

DRIVER_INITIALIZE DriverEntry;
DRIVER_UNLOAD     DriverUnload;
DRIVER_DISPATCH   CbIoctlDeviceControl;
PVOID             ObRegistrationHandle;
PEPROCESS         ProtectedProcess;

void
DriverUnload(
    _In_ PDRIVER_OBJECT DriverObject
)
{
    PDEVICE_OBJECT deviceObject = DriverObject->DeviceObject;
    UNICODE_STRING dosDeviceName;

    PAGED_CODE();

    RtlInitUnicodeString(&dosDeviceName, DOS_DEVICE_NAME);
    IoDeleteSymbolicLink(&dosDeviceName);

    ObUnRegisterCallbacks(ObRegistrationHandle);

    if (deviceObject != NULL)
    {
        IoDeleteDevice(deviceObject);
    }
}

NTSTATUS
CbIoctlCreateClose(
    _In_ PDEVICE_OBJECT DeviceObject,
    _In_ PIRP Irp
)
{
    UNREFERENCED_PARAMETER(DeviceObject);
     
    PAGED_CODE();

    Irp->IoStatus.Status = STATUS_SUCCESS;
    Irp->IoStatus.Information = 0;

    IoCompleteRequest(Irp, IO_NO_INCREMENT);

    return STATUS_SUCCESS;
}

NTSTATUS
CbIoctlDeviceControl(
    _In_    PDEVICE_OBJECT DeviceObject,
    _Inout_ PIRP		   Irp
)
{
    PIO_STACK_LOCATION irpSp;
    NTSTATUS		   ntStatus = STATUS_SUCCESS;
    ULONG			   ioControlCode;
    PCHAR              inBuf;
    ULONG              inBufLength;
    HANDLE             processId;

    UNREFERENCED_PARAMETER(DeviceObject);

    PAGED_CODE();

    irpSp = IoGetCurrentIrpStackLocation(Irp);
    ioControlCode = irpSp->Parameters.DeviceIoControl.IoControlCode;

    switch (ioControlCode)
    {
    case IOCTL_PROTECT_PROCESS:
    {
        inBuf = Irp->AssociatedIrp.SystemBuffer;
        inBufLength = irpSp->Parameters.DeviceIoControl.InputBufferLength;
        if (inBufLength != sizeof(processId))
        {
            ntStatus = STATUS_INVALID_BUFFER_SIZE;
            break;
        }
        RtlCopyBytes(&processId, inBuf, inBufLength);
        ntStatus = PsLookupProcessByProcessId(processId, &ProtectedProcess);
        if (!NT_SUCCESS(ntStatus))
        {
            ntStatus = STATUS_INVALID_ADDRESS;
            break;
        }
    }
    default:
    {
        ntStatus = STATUS_INVALID_DEVICE_REQUEST;
        break;
    }
    }

    Irp->IoStatus.Status = ntStatus;
    IoCompleteRequest(Irp, IO_NO_INCREMENT);

    return STATUS_SUCCESS;
}

OB_PREOP_CALLBACK_STATUS
CbPreProcessCreate(
    _In_    PVOID                         RegistrationContext,
    _Inout_ POB_PRE_OPERATION_INFORMATION OperationInformation
)
{
    UNREFERENCED_PARAMETER(RegistrationContext);

    if (OperationInformation->KernelHandle == TRUE)
    {
        return OB_PREOP_SUCCESS;
    }

    if (OperationInformation->ObjectType == *PsProcessType)
    {
        if (ProtectedProcess == OperationInformation->Object)
        {
            OperationInformation->Parameters->CreateHandleInformation.DesiredAccess &= ~PROCESS_TERMINATE;
        }
    }

    return OB_PREOP_SUCCESS;
}

VOID
CbPostProcessCreate(
    _In_ PVOID                          RegistrationContext,
    _In_ POB_POST_OPERATION_INFORMATION OperationInformation
)
{
    UNREFERENCED_PARAMETER(RegistrationContext);
    UNREFERENCED_PARAMETER(OperationInformation);
}

NTSTATUS
DriverEntry(
    _In_ PDRIVER_OBJECT	 DriverObject,
    _In_ PUNICODE_STRING RegistryPath
)
{
    NTSTATUS                  ntStatus         = STATUS_SUCCESS;
    UNICODE_STRING            ntDeviceName     = { 0 };
    UNICODE_STRING            dosDeviceName    = { 0 };
    PDEVICE_OBJECT            deviceObject     = NULL;
    OB_CALLBACK_REGISTRATION  obCbRegistration = { 0 };
    OB_OPERATION_REGISTRATION obOpRegistration = { 0 };
    UNICODE_STRING            obCbAltitude     = { 0 };

    UNREFERENCED_PARAMETER(RegistryPath);

    RtlInitUnicodeString(&ntDeviceName, NT_DEVICE_NAME);

    ntStatus = IoCreateDevice(
        DriverObject,
        0,
        &ntDeviceName,
        FILE_DEVICE_UNKNOWN,
        FILE_DEVICE_SECURE_OPEN,
        FALSE,
        &deviceObject);
    if (!NT_SUCCESS(ntStatus))
    {
        MyDbgPrint("IoCreateDevice error: 0x%lx", ntStatus);
        return ntStatus;
    }

    DriverObject->DriverUnload = DriverUnload;
    DriverObject->MajorFunction[IRP_MJ_CREATE] = CbIoctlCreateClose;
    DriverObject->MajorFunction[IRP_MJ_CLOSE] = CbIoctlCreateClose;
    DriverObject->MajorFunction[IRP_MJ_DEVICE_CONTROL] = CbIoctlDeviceControl;

    RtlInitUnicodeString(&obCbAltitude, L"1000");

    obOpRegistration.ObjectType = PsProcessType;
    obOpRegistration.Operations = OB_OPERATION_HANDLE_CREATE;
    obOpRegistration.PreOperation = CbPreProcessCreate;
    obOpRegistration.PostOperation = CbPostProcessCreate;

    obCbRegistration.Version = OB_FLT_REGISTRATION_VERSION;
    obCbRegistration.OperationRegistrationCount = 1;
    obCbRegistration.Altitude = obCbAltitude;
    obCbRegistration.RegistrationContext = NULL;
    obCbRegistration.OperationRegistration = &obOpRegistration;

    ntStatus = ObRegisterCallbacks(&obCbRegistration, &ObRegistrationHandle);
    if (!NT_SUCCESS(ntStatus))
    {
        MyDbgPrint("ObRegisterCallbacks error: 0x%lx", ntStatus);
        IoDeleteDevice(deviceObject);
        return ntStatus;
    }

    RtlInitUnicodeString(&dosDeviceName, DOS_DEVICE_NAME);

    ntStatus = IoCreateSymbolicLink(&dosDeviceName, &ntDeviceName);
    if (!NT_SUCCESS(ntStatus))
    {
        MyDbgPrint("IoCreateSymbolicLink error: 0x%lx", ntStatus);
        ObUnRegisterCallbacks(ObRegistrationHandle);
        IoDeleteDevice(deviceObject);
        return ntStatus;
    }

    return ntStatus;
}
