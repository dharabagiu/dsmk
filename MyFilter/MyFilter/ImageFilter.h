#ifndef _IMAGE_FILTER_HPP_INCLUDED_
#define _IMAGE_FILTER_HPP_INCLUDED_
//

//   Author(s)    : Radu PORTASE(rportase@bitdefender.com)
//

#include "MyDriver.h"

NTSTATUS
ImageFltInitialize();

NTSTATUS
ImageFltUninitialize();

#endif