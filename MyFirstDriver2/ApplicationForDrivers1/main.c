#include <Windows.h>
#include <winioctl.h>
#include <stdio.h>

#define IOCTL_CUSTOM_1 CTL_CODE(FILE_DEVICE_UNKNOWN, 0xd80, METHOD_NEITHER, FILE_ANY_ACCESS)
#define IOCTL_CUSTOM_2 CTL_CODE(FILE_DEVICE_UNKNOWN, 0xd81, METHOD_NEITHER, FILE_ANY_ACCESS)

int main()
{
	HANDLE hDevice = CreateFile(
		"\\\\.\\Driver1",
		GENERIC_READ | GENERIC_WRITE,
		FILE_SHARE_READ | FILE_SHARE_WRITE,
		NULL,
		OPEN_EXISTING,
		FILE_FLAG_OVERLAPPED,
		NULL);

	if (hDevice == INVALID_HANDLE_VALUE)
	{
		printf_s("CreateFile error: %u\n", GetLastError());
		return 0;
	}

	BOOL result = DeviceIoControl(
		hDevice,
		IOCTL_CUSTOM_1,
		NULL,
		0,
		NULL,
		0,
		NULL,
		NULL);
	if (!result)
	{
		printf_s("DeviceIoControl error: %u\n", GetLastError());
	}

	result = DeviceIoControl(
		hDevice,
		IOCTL_CUSTOM_2,
		NULL,
		0,
		NULL,
		0,
		NULL,
		NULL);
	if (!result)
	{
		printf_s("DeviceIoControl error: %u\n", GetLastError());
	}

	CloseHandle(hDevice);

	return 0;
}