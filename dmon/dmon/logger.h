#pragma once

#include <stdio.h>

#define LOG_LEVEL_DEBUG    0
#define LOG_LEVEL_INFO     1
#define LOG_LEVEL_WARNING  2
#define LOG_LEVEL_ERROR    3
#define LOG_LEVEL_CRITICAL 4
#define LOG_LEVEL_FATAL    5

#define LogDebug(format, ...) \
        LogPrint(LOG_LEVEL_DEBUG, __FILE__, __LINE__, format, __VA_ARGS__)
#define LogInfo(format, ...) \
        LogPrint(LOG_LEVEL_INFO, __FILE__, __LINE__, format, __VA_ARGS__)
#define LogWarning(format, ...) \
        LogPrint(LOG_LEVEL_WARNING, __FILE__, __LINE__, format, __VA_ARGS__)
#define LogError(format, ...) \
        LogPrint(LOG_LEVEL_ERROR, __FILE__, __LINE__, format, __VA_ARGS__)
#define LogCritical(format, ...) \
        LogPrint(LOG_LEVEL_CRITICAL, __FILE__, __LINE__, format, __VA_ARGS__)
#define LogFatal(format, ...) \
        LogPrint(LOG_LEVEL_CRITICAL, __FILE__, __LINE__, format, __VA_ARGS__)

void LoggerEnable();
void LoggerDisable();
void LoggerSetOutput(FILE* File);
void LoggerSetMinLevel(int Level);

void LogPrint(int Level, const char* File, int Line, const char* Format, ...);
