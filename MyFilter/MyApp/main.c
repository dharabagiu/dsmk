//
//   Author(s)    : Radu PORTASE(rportase@bitdefender.com)
//
#include "driver_commands.h"
#include "globaldata.h"
#include <stdio.h>

#define COMMAND_LINE_LENGTH 1024
#define COMMAND_LINE_DELIM " \t\n"

APP_GLOBAL_DATA gApp;

MONITORING_TYPE StrToMonitoringType(const char* Str, size_t Max)
{
    MONITORING_TYPE invalidValue = (MONITORING_TYPE)-1;
    if (Str == NULL)
    {
        return invalidValue;;
    }
    if (!_strnicmp(Str, "process", Max))
    {
        return monitoringProcess;
    }
    if (!_strnicmp(Str, "thread", Max))
    {
        return monitoringThreads;
    }
    if (!_strnicmp(Str, "image", Max))
    {
        return monitoringImage;
    }
    if (!_strnicmp(Str, "registry", Max))
    {
        return monitoringRegistry;
    }
    if (!_strnicmp(Str, "file", Max))
    {
        return monitoringFiles;
    }
    return invalidValue;
}

int 
__cdecl
main(
    int argc,
    char *argv[]
)
{
    UNREFERENCED_PARAMETER(argc);
    UNREFERENCED_PARAMETER(argv);

    CommDriverPreinitialize();
    NTSTATUS status = CommDriverInitialize();
    if (status < 0)
    {
        return status;
    }
	
    printf_s("Bine ai venit in aplicatia mea puscata!\n");
    printf_s("Te rog scrie o comanda si eu o voi executa ca un sclav bun ce sunt.\n");

    int running = 1;
    char commandLine[COMMAND_LINE_LENGTH];
    char* commandStrtokContext;
    while (running)
    {
        printf_s("\n> ");
        fgets(commandLine, COMMAND_LINE_LENGTH, stdin);
        char* command = strtok_s(commandLine, COMMAND_LINE_DELIM, &commandStrtokContext);
        if (command == NULL)
        {
            printf_s("Mai incearca\n");
            continue;
        }
        if (!_strnicmp(command, "start", COMMAND_LINE_LENGTH))
        {
            char* strMonitoringType = strtok_s(NULL, COMMAND_LINE_DELIM, &commandStrtokContext);
            MONITORING_TYPE monitoringType = StrToMonitoringType(strMonitoringType, COMMAND_LINE_LENGTH - (strMonitoringType - command));
            if (monitoringType == (MONITORING_TYPE)-1)
            {
                printf_s("Mai incearca\n");
                continue;
            }
            status = CmdStartMonitoring(monitoringType);
            printf("Start monitoring returned status = 0x%X\n", status);
        }
        else if (!_strnicmp(command, "stop", COMMAND_LINE_LENGTH))
        {
            char* strMonitoringType = strtok_s(NULL, COMMAND_LINE_DELIM, &commandStrtokContext);
            MONITORING_TYPE monitoringType = StrToMonitoringType(strMonitoringType, COMMAND_LINE_LENGTH - (strMonitoringType - command));
            if (monitoringType == (MONITORING_TYPE)-1)
            {
                printf_s("Mai incearca\n");
                continue;
            }
            status = CmdStopMonitoring(monitoringType);
            printf("Stop monitoring returned status = 0x%X\n", status);
        }
        else if (!_strnicmp(command, "exit", COMMAND_LINE_LENGTH))
        {
            running = 0;
        }
        else
        {
            printf_s("Mai incearca\n");
        }
    }

    CmdStopMonitoring(monitoringProcess);
    CmdStopMonitoring(monitoringThreads);
    CmdStopMonitoring(monitoringImage);
    CmdStopMonitoring(monitoringRegistry);
    CmdStopMonitoring(monitoringFiles);

    CommDriverUninitialize();
}