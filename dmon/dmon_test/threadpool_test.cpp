#include "CppUnitTest.h"

extern "C"
{
#include "threadpool.h"
}

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace dmon_test
{
    struct WorkParameter
    {
        bool workExecuted;
        int shouldReturn;
    };

    int WorkFunction(void* param)
    {
        WorkParameter* _param = static_cast<WorkParameter*>(param);
        _param->workExecuted = true;
        return _param->shouldReturn;
    }

	TEST_CLASS(ThreadPoolTest)
	{
	public:
		
		TEST_METHOD(Initialize_SmallNumberOfThreads)
		{
            auto threadPool = ThreadPoolInit(4);
            Assert::IsNotNull(threadPool);
            ThreadPoolUninit(threadPool);
		}

        TEST_METHOD(Initialize_LargeNumberOfThreads)
        {
            auto threadPool = ThreadPoolInit(64);
            Assert::IsNotNull(threadPool);
            ThreadPoolUninit(threadPool);
        }

        TEST_METHOD(Initialize_InvalidNumberOfThreads)
        {
            auto threadPool = ThreadPoolInit(0);
            Assert::IsNull(threadPool);
        }

        TEST_METHOD(AddWork_OneWork_OneThread)
        {
            auto threadPool = ThreadPoolInit(1);
            Assert::IsNotNull(threadPool);

            WorkParameter param{ false, 1 };
            int exitStatus;

            WORK work{ &WorkFunction, &param, &exitStatus };
            auto event = ThreadPoolAddWork(threadPool, (PCWORK)&work);
            Assert::IsNotNull(event);

            Assert::AreEqual(WAIT_OBJECT_0, WaitForSingleObject(event, kWaitTimeoutMs));
            CloseHandle(event);

            Assert::IsTrue(param.workExecuted);
            Assert::AreEqual(param.shouldReturn, exitStatus);
        }

        TEST_METHOD(AddWork_OneWork_OneMultipleThreads)
        {
            auto threadPool = ThreadPoolInit(8);
            Assert::IsNotNull(threadPool);

            WorkParameter param{ false, 1 };
            int exitStatus;

            WORK work{ &WorkFunction, &param, &exitStatus };
            auto event = ThreadPoolAddWork(threadPool, (PCWORK)&work);
            Assert::IsNotNull(event);

            Assert::AreEqual(WAIT_OBJECT_0, WaitForSingleObject(event, kWaitTimeoutMs));
            CloseHandle(event);

            Assert::IsTrue(param.workExecuted);
            Assert::AreEqual(param.shouldReturn, exitStatus);
        }

        TEST_METHOD(AddWork_MultipleWorks_OneThread)
        {
            auto threadPool = ThreadPoolInit(1);
            Assert::IsNotNull(threadPool);

            constexpr int numberOfWorks = 100;

            WorkParameter params[numberOfWorks];
            int exitStatus[numberOfWorks];

            WORK work[numberOfWorks];
            HANDLE events[numberOfWorks];

            for (int i = 0; i != numberOfWorks; ++i)
            {
                params[i] = WorkParameter{ false, i + 1 };
                work[i] = WORK{ &WorkFunction, &params[i], &exitStatus[i] };
                events[i] = ThreadPoolAddWork(threadPool, (PCWORK)&work[i]);
                Assert::IsNotNull(events[i]);
            }

            for (int i = 0; i != numberOfWorks; ++i)
            {
                Assert::AreEqual(WAIT_OBJECT_0, WaitForSingleObject(events[i], kWaitTimeoutMs));
                CloseHandle(events[i]);
                Assert::IsTrue(params[i].workExecuted);
                Assert::AreEqual(params[i].shouldReturn, exitStatus[i]);
            }
        }

        TEST_METHOD(AddWork_MultipleWorks_MultipleThreads)
        {
            auto threadPool = ThreadPoolInit(8);
            Assert::IsNotNull(threadPool);

            constexpr int numberOfWorks = 100;

            WorkParameter params[numberOfWorks];
            int exitStatus[numberOfWorks];

            WORK work[numberOfWorks];
            HANDLE events[numberOfWorks];

            for (int i = 0; i != numberOfWorks; ++i)
            {
                params[i] = WorkParameter{ false, i + 1 };
                work[i] = WORK{ &WorkFunction, &params[i], &exitStatus[i] };
                events[i] = ThreadPoolAddWork(threadPool, (PCWORK)&work[i]);
                Assert::IsNotNull(events[i]);
            }

            for (int i = 0; i != numberOfWorks; ++i)
            {
                Assert::AreEqual(WAIT_OBJECT_0, WaitForSingleObject(events[i], kWaitTimeoutMs));
                CloseHandle(events[i]);
                Assert::IsTrue(params[i].workExecuted);
                Assert::AreEqual(params[i].shouldReturn, exitStatus[i]);
            }
        }

    private:
        static const DWORD kWaitTimeoutMs = 100;
	};
}
