#include <stdio.h>
#include <stdlib.h>
#include "commands.h"
#include "dmon.h"

void CmdHelp()
{
    printf("\n"
           "help  - Shows information about all commands\n"
           "exit  - Exit dmon\n"
           "start - Start dmon service\n"
           "stop  - Stop dmon service\n"
           "\n");
}

void CmdExit()
{
    StopDmon();
    exit(0);
}

void CmdStart()
{
    if (DmonIsRunning())
    {
        printf("Dmon is already running.\n\n");
    }

    if (!StartDmon())
    {
        printf("Dmon could not be started. See the log file for details.\n\n");
    }
}

void CmdStop()
{
    if (!DmonIsRunning())
    {
        printf("Dmon is not running.\n\n");
    }

    StopDmon();
}

void CmdPs()
{
    if (!DmonIsRunning())
    {
        printf("Dmon is not running.\n\n");
        return;
    }

    if (!PrintRunningProcesses(stdout))
    {
        printf("An error occured while performing this operation. See the log file for details.\n\n");
    }
}

void CmdProtect(uint32_t processId)
{
    if (!DmonIsRunning())
    {
        printf("Dmon is not running.\n\n");
        return;
    }

    if (!ProtectProcess((HANDLE)processId))
    {
        printf("An error occured while performing this operation. See the log file for details.\n\n");
    }
}
