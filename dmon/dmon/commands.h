#pragma once

#include <stdint.h>

void CmdHelp();
void CmdExit();
void CmdStart();
void CmdStop();
void CmdPs();
void CmdProtect(uint32_t processId);
