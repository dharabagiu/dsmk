#include <WS2tcpip.h>
#include <fwpmu.h>
#include <WinSock2.h>
#include <stdio.h>
#include <stdlib.h>

#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Fwpuclnt.lib")

#define UNUSED(x) ((void)(x))
#define CHROME_PATH L"C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe"

int main(int argc, char** argv)
{
	if (argc != 3)
	{
		printf("Usage: %s [host] [port]\n", argv[0]);
		return 0;
	}

	WSADATA wsaData;
	{
		int retval = WSAStartup(MAKEWORD(2, 2), &wsaData);
		if (retval != 0)
		{
			printf("WSAStartup error: %d\n", retval);
			return 0;
		}
	}
	

	ADDRINFOA hints = { 0 };
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

	PADDRINFOA result;
	{
		INT retval = getaddrinfo(argv[1], argv[2], &hints, &result);
		if (retval != 0)
		{
			printf("getaddrinfo error: %s\n", gai_strerrorA(retval));
			return 0;
		}
	}

	HANDLE engine;
	{
		DWORD retval = FwpmEngineOpen0(NULL, RPC_C_AUTHN_WINNT, NULL, NULL, &engine);
		if (retval != ERROR_SUCCESS)
		{
			printf("FwpmEngineOpen0 error: %lx\n", retval);
			return 0;
		}
	}

	FWPM_FILTER_CONDITION0* conditions[8];
	INT numConditions = 0;
	FWP_V4_ADDR_AND_MASK* ipmasks[8];
	INT numIpmasks = 0;
	UINT64 filterIds[8];
	INT numFilterIds = 0;

	FWP_BYTE_BLOB* appByteBlob = NULL;
	{
		DWORD retval = FwpmGetAppIdFromFileName0(CHROME_PATH, &appByteBlob);
		if (retval != ERROR_SUCCESS)
		{
			printf("FwpmGetAppIdFromFileName0 error: %lx\n", retval);
			return 0;
		}
	}

	for (PADDRINFOA addrinfo = result; addrinfo != NULL; addrinfo = addrinfo->ai_next)
	{
		if (addrinfo->ai_family != AF_INET)
		{
			continue;
		}
		FWPM_FILTER0 filter = { 0 };
		filter.layerKey = FWPM_LAYER_ALE_AUTH_CONNECT_V4;
		filter.action.type = FWP_ACTION_BLOCK;
		filter.weight.type = FWP_EMPTY;
		filter.displayData.name = L"PLM";
		
		ipmasks[numIpmasks] = (FWP_V4_ADDR_AND_MASK*)malloc(sizeof(FWP_V4_ADDR_AND_MASK));
		if (ipmasks[numIpmasks] == NULL)
		{
			printf("Out of memory\n");
			return 0;
		}
		struct sockaddr_in* addr = (struct sockaddr_in*)addrinfo->ai_addr;
		ipmasks[numIpmasks]->addr = ntohl(addr->sin_addr.S_un.S_addr);
		ipmasks[numIpmasks]->mask = 0xffffffff;

		conditions[numConditions] = (FWPM_FILTER_CONDITION0*)calloc(2, sizeof(FWPM_FILTER_CONDITION0));
		if (conditions[numConditions] == NULL)
		{
			printf("Out of memory\n");
			return 0;
		}

		conditions[numConditions][0].fieldKey = FWPM_CONDITION_IP_REMOTE_ADDRESS;
		conditions[numConditions][0].matchType = FWP_MATCH_EQUAL;
		conditions[numConditions][0].conditionValue.type = FWP_V4_ADDR_MASK;
		conditions[numConditions][0].conditionValue.v4AddrMask = ipmasks[numIpmasks];

		conditions[numConditions][1].fieldKey = FWPM_CONDITION_ALE_APP_ID;
		conditions[numConditions][1].matchType = FWP_MATCH_EQUAL;
		conditions[numConditions][1].conditionValue.type = FWP_BYTE_BLOB_TYPE;
		conditions[numConditions][1].conditionValue.byteBlob = appByteBlob;

		filter.numFilterConditions = 2;
		filter.filterCondition = conditions[numConditions];

		DWORD retval = FwpmFilterAdd0(engine, &filter, NULL, &filterIds[numFilterIds]);
		if (retval != ERROR_SUCCESS)
		{
			printf("FwpmFilterAdd0 error: %lx\n", retval);
			return 0;
		}

		++numConditions;
		++numFilterIds;
		++numIpmasks;
	}

	printf("Filtering started. Press any key to stop...\n");
	int c = getchar();
	UNUSED(c);

	for (INT i = 0; i != numFilterIds; ++i)
	{
		FwpmFilterDeleteById0(engine, filterIds[i]);
	}
	FwpmEngineClose0(engine);
	freeaddrinfo(result);

	for (INT i = 0; i != numConditions; ++i)
	{
		free(conditions[i]);
	}
	for (INT i = 0; i != numIpmasks; ++i)
	{
		free(ipmasks[i]);
	}

	WSACleanup();

	return 0;
}