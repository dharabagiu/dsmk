#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdint.h>
#include "common.h"
#include "commands.h"
#include "logger.h"

#define MAX_COMMAND 512

int main(int argc, char** argv)
{
    UNUSED_PARAMETER(argc);
    UNUSED_PARAMETER(argv);

    char logFileName[32];
    sprintf_s(logFileName, 32, "dmon-%lu.log", (unsigned long)time(NULL));

    FILE* logFile;
    errno_t error = fopen_s(&logFile, logFileName, "w");

    LoggerEnable();
    if (0 != error)
    {
        char errorMsg[MAX_ERROR_MSG];
        strerror_s(errorMsg, MAX_ERROR_MSG, error);
        LogError("Could not open log file %s. Logging to standard output. Error message: %s",
            "dmon.log", errorMsg);
    }
    else
    {
        LoggerSetOutput(logFile);
    }

    char command[MAX_COMMAND];
    for (;;)
    {
        printf("dmon$ ");
        fgets(command, MAX_COMMAND, stdin);
        if (0 != command[0])
        {
            command[strlen(command) - 1] = 0;
        }

        LogDebug("User command: %s", command);

        char* split_context = NULL;
        char* command_name = strtok_s(command, " \t", &split_context);

        if (0 == strncmp("help", command_name, MAX_COMMAND))
        {
            CmdHelp();
        }
        else if (0 == strncmp("exit", command_name, MAX_COMMAND))
        {
            CmdExit();
        }
        else if (0 == strncmp("start", command_name, MAX_COMMAND))
        {
            CmdStart();
        }
        else if (0 == strncmp("stop", command_name, MAX_COMMAND))
        {
            CmdStop();
        }
        else if (0 == strncmp("ps", command_name, MAX_COMMAND))
        {
            CmdPs();
        }
        else if (0 == strncmp("protect", command_name, MAX_COMMAND))
        {
            char* process_id_str = strtok_s(NULL, command, &split_context);
            uint32_t process_id;
            sscanf_s(process_id_str, "%u", &process_id);
            CmdProtect(process_id);
        }
        else
        {
            printf("Unknown command \"%s\"\n\n", command_name);
        }
    }

    fclose(logFile);

    return 0;
}
