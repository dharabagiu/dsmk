#pragma once

#include <Windows.h>
#include <stdio.h>

BOOL DmonIsRunning();
BOOL StartDmon();
void StopDmon();
BOOL PrintRunningProcesses(FILE* file);
BOOL ProtectProcess(HANDLE processId);
