#pragma once

#include <stddef.h>

typedef struct _LINKED_LIST LINKED_LIST;
typedef struct _LINKED_LIST *PLINKED_LIST;
typedef struct _LINKED_LIST const* PCLINKED_LIST;
typedef struct _LL_NODE LL_NODE;
typedef struct _LL_NODE *PLL_NODE;
typedef struct _LL_NODE const* PCLL_NODE;

PLINKED_LIST LLInit(size_t ElementSize);
void LLUninit(PLINKED_LIST List);
PLL_NODE LLBegin(PCLINKED_LIST List);
PLL_NODE LLEnd(PCLINKED_LIST List);
PLL_NODE LLNext(PCLL_NODE Node);
PLL_NODE LLPrevious(PCLL_NODE Node);
int LLInsert(PLINKED_LIST List, PLL_NODE After, const void* Element);
int LLRemove(PLINKED_LIST List, PLL_NODE Node);
size_t LLSize(PCLINKED_LIST List);
void* LLGet(PCLL_NODE Node);
