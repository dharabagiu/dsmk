#pragma once

#include <Windows.h>

typedef int(*WORK_PROCEDURE)(void*);
typedef struct _THREAD_POOL THREAD_POOL;
typedef struct _THREAD_POOL *PTHREAD_POOL;

typedef struct _WORK
{
    WORK_PROCEDURE Proc;
    void*          Params;
    int*           ExitStatus;
} WORK;
typedef struct _WORK *PWORK;
typedef struct _WORK const *PCWORK;

PTHREAD_POOL ThreadPoolInit(size_t NumberOfThreads);
void ThreadPoolUninit(PTHREAD_POOL ThreadPool);
HANDLE ThreadPoolAddWork(PTHREAD_POOL ThreadPool, PCWORK Work);
