#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "linkedlist.h"
#include "logger.h"

struct _LINKED_LIST
{
    LL_NODE* First;
    LL_NODE* Last;
    size_t   Size;
    size_t   ElementSize;
};

struct _LL_NODE
{
    struct _LL_NODE* Prev;
    struct _LL_NODE* Next;
    void*            Data;
};

PLINKED_LIST LLInit(size_t ElementSize)
{
    PLINKED_LIST list = (PLINKED_LIST)malloc(sizeof(LINKED_LIST));
    if (NULL == list)
    {
        LogFatal("Could not allocate memory");
        assert(0);
    }

    list->First = NULL;
    list->Last = NULL;
    list->Size = 0;
    list->ElementSize = ElementSize;

    return list;
}

void LLUninit(PLINKED_LIST List)
{
    if (NULL == List)
    {
        LogError("Parameter List cannot be NULL");
        return;
    }

    for (LL_NODE* it = LLBegin(List); NULL != it;)
    {
        LL_NODE* next = LLNext(it);
        free(it->Data);
        free(it);
        it = next;
    }

    free(List);
}

PLL_NODE LLBegin(PCLINKED_LIST List)
{
    if (NULL == List)
    {
        LogError("Parameter List cannot be NULL");
        return NULL;
    }
    return List->First;
}

PLL_NODE LLEnd(PCLINKED_LIST List)
{
    if (NULL == List)
    {
        LogError("Parameter List cannot be NULL");
        return NULL;
    }
    return List->Last;
}

PLL_NODE LLNext(PCLL_NODE Node)
{
    if (NULL == Node)
    {
        LogError("Parameter Node cannot be NULL");
        return NULL;
    }
    return Node->Next;
}

PLL_NODE LLPrevious(PCLL_NODE Node)
{
    if (NULL == Node)
    {
        LogError("Parameter Node cannot be NULL");
        return NULL;
    }
    return Node->Prev;
}

int LLInsert(PLINKED_LIST List, PLL_NODE After, const void* Element)
{
    if (NULL == List)
    {
        LogError("Parameter List cannot be NULL");
        return 0;
    }
    if (NULL == Element)
    {
        LogError("Parameter Element cannot be NULL");
        return 0;
    }

    LL_NODE* newNode = (LL_NODE*)malloc(sizeof(LL_NODE));
    if (NULL == newNode)
    {
        LogFatal("Could not allocate memory");
        assert(0);
    }

    newNode->Data = malloc(List->ElementSize);
    if (NULL == newNode->Data)
    {
        LogFatal("Could not allocate memory");
        assert(0);
    }

    memcpy(newNode->Data, Element, List->ElementSize);
    newNode->Prev = After;

    if (NULL == After)
    {
        newNode->Next = List->First;
        if (NULL != List->First)
        {
            List->First->Prev = newNode;
        }
        else
        {
            List->Last = newNode;
        }
        List->First = newNode;
    }
    else
    {
        newNode->Next = After->Next;
        if (NULL != After->Next)
        {
            After->Next->Prev = newNode;
        }
        else
        {
            List->Last = newNode;
        }
        After->Next = newNode;
    }

    ++List->Size;

    return 1;
}

int LLRemove(PLINKED_LIST List, PLL_NODE Node)
{
    if (NULL == List)
    {
        LogError("Parameter List cannot be NULL");
        return 0;
    }
    if (NULL == Node)
    {
        LogError("Parameter Node cannot be NULL");
        return 0;
    }

    if (NULL == Node->Prev)
    {
        List->First = Node->Next;
    }
    else
    {
        Node->Prev->Next = Node->Next;
    }

    if (NULL == Node->Next)
    {
        List->Last = Node->Prev;
    }
    else
    {
        Node->Next->Prev = Node->Prev;
    }

    free(Node->Data);
    free(Node);

    --List->Size;

    return 1;
}

size_t LLSize(PCLINKED_LIST List)
{
    if (NULL == List)
    {
        LogError("Parameter List cannot be NULL");
        return 0;
    }
    return List->Size;
}

void* LLGet(PCLL_NODE Node)
{
    if (NULL == Node)
    {
        LogError("Parameter Node cannot be NULL");
        return NULL;
    }
    return Node->Data;
}
