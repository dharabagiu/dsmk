//
//   Author(s)    : Radu PORTASE(rportase@bitdefender.com)
//
#include "RegistryFilter.h"
#include "Communication.h"
#include "CommShared.h"
#include "Trace.h"
#include "RegistryFilter.tmh"
#include <ntstrsafe.h>

static LARGE_INTEGER RegistryFltNotifyRoutineCookie;

static VOID
RegistryFltSendMessage(
    _In_ PVOID OperationType,
    _In_ PCUNICODE_STRING KeyPath,
    _In_ PVOID Extra
)
{
    WCHAR descriptionBuffer[1024];
    UNICODE_STRING description;
    description.Buffer = descriptionBuffer;
    description.MaximumLength = sizeof(descriptionBuffer);

    if (OperationType == (PVOID)RegNtPreCreateKey)
    {
        RtlUnicodeStringPrintf(&description, L"Operation = CreateKey, Path = %wZ", KeyPath);
    }
    else if (OperationType == (PVOID)RegNtPreSetValueKey)
    {
        PREG_SET_VALUE_KEY_INFORMATION info = (PREG_SET_VALUE_KEY_INFORMATION)Extra;
        RtlUnicodeStringPrintf(&description, L"Operation = SetValue, Path = %wZ, ValueName = %wZ", KeyPath, info->ValueName);
    }
    else if (OperationType == (PVOID)RegNtPreDeleteKey)
    {
        RtlUnicodeStringPrintf(&description, L"Operation = DeleteKey, Path = %wZ", KeyPath);
    }
    else if (OperationType == (PVOID)RegNtPreDeleteValueKey)
    {
        PREG_DELETE_VALUE_KEY_INFORMATION info = (PREG_DELETE_VALUE_KEY_INFORMATION)Extra;
        RtlUnicodeStringPrintf(&description, L"Operation = DeleteValue, Path = %wZ, ValueName = %wZ", KeyPath, info->ValueName);
    }
    else if (OperationType == (PVOID)RegNtPreRenameKey)
    {
        PREG_RENAME_KEY_INFORMATION info = (PREG_RENAME_KEY_INFORMATION)Extra;
        RtlUnicodeStringPrintf(&description, L"Operation = RenameKey, Path = %wZ, NewName = %wZ", KeyPath, info->NewName);
    }
    else
    {
        return;
    }

    PMY_DRIVER_REGISTRY_OPERATION_MSG pMsg = NULL;

    ULONG msgSize = sizeof(MY_DRIVER_REGISTRY_OPERATION_MSG);
    msgSize += description.Length;

    pMsg = ExAllocatePoolWithTag(PagedPool, msgSize, 'GSM+');
    if (!pMsg)
    {
        return;
    }

    pMsg->Header.MessageCode = msgRegistryOperation;
    pMsg->OperationDescriptionLength = description.Length;
    RtlCopyMemory(&pMsg->Data[0], description.Buffer, description.Length);

    ULONG replySize = 0;
    NTSTATUS status = CommSendMessage(pMsg, msgSize, NULL, &replySize);
    if (!NT_SUCCESS(status))
    {
        LogError("CommSendMessage failed with status = 0x%X", status);
    }

    ExFreePoolWithTag(pMsg, 'GSM+');
}

static NTSTATUS
RegistryFltNotifyRoutine(
    PVOID CallbackContext,
    PVOID Argument1,
    PVOID Argument2
)
{
    UNREFERENCED_PARAMETER(CallbackContext);

    NTSTATUS ntStatus = STATUS_SUCCESS;

    if (Argument2 == NULL)
    {
        return ntStatus;
    }

    PCUNICODE_STRING registryKeyPath;

    ntStatus = CmCallbackGetKeyObjectIDEx(
        &RegistryFltNotifyRoutineCookie,
        *(PVOID*)Argument2, // HACK
        NULL,
        &registryKeyPath,
        0);
    if (!NT_SUCCESS(ntStatus))
    {
        LogError("CmCallbackGetKeyObjectIDEx error %ld", ntStatus);
        return ntStatus;
    }

    RegistryFltSendMessage(Argument1, registryKeyPath, Argument2);

    return ntStatus;
}

NTSTATUS
RegistryFltInitialize()
{
    return CmRegisterCallback(RegistryFltNotifyRoutine, NULL, &RegistryFltNotifyRoutineCookie);
}

NTSTATUS
RegistryFltUninitialize()
{
    return CmUnRegisterCallback(RegistryFltNotifyRoutineCookie);
}
